#!/bin/bash

mkdir -p build
x=$1
y=$2
z=$3

# Accepts first three arguments and assigns them respectively

res1=$((x**y))

res=$((res1+z*y))

read -p 'Would you like to change the file name? Enter yes or no: ' a

if [[ "$a" == "yes" ]]; then
    ext=".txt"
    read -p 'Enter new filename: ' OUTPUT_FILE_NAME
    OUTPUT_FILE_NAME="$OUTPUT_FILE_NAME$ext"
else
    OUTPUT_FILE_NAME="myresult.txt"
    echo " "
    echo "Filename not changed. Your answer was saved to the default file $OUTPUT_FILE_NAME"
fi


echo $res >> build/$OUTPUT_FILE_NAME

echo " "
echo "The following was saved to the file $OUTPUT_FILE_NAME : ($x ^ $y) + $z x $y = $res"




# 


